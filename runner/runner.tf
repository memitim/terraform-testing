variable "ami" {}
variable "subnet_id" {}
variable "instance_type" {}
variable "vpc_security_group_id" {}
variable "identity" {}
variable "region" {}
variable "runner_token" {}
variable "key_name" {}

provider "aws" {
  region = "${var.region}"
}
 
resource "aws_instance" "gitlab-runner" {
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "tfdeploys"
  vpc_security_group_ids = ["${var.vpc_security_group_id}"]
  tags {
    Name = "gitlab-runner"
  }
 
  provisioner "remote-exec" {
  connection {
    type     = "ssh"
    user     = "core"
    private_key = "${var.key_name}"
    }
 
    inline = [
      "docker run -d -e DOCKER_IMAGE=ruby:2.1 -e RUNNER_NAME=terraform-runner -e CI_SERVER_URL=https://gitlab.com/ -e REGISTRATION_TOKEN=${var.runner_token} -e RUNNER_EXECUTOR=docker -e REGISTER_NON_INTERACTIVE=true --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:alpine-v10.8.0",
      "docker exec -it gitlab-runner gitlab-runner register"
    ]
  }
}

output "public_ip" {
  value = "${aws_instance.gitlab-runner.public_ip}"
}

output "public_dns" {
  value = "${aws_instance.gitlab-runner.public_dns}"
}