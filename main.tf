variable "ami" {}
variable "subnet_id" {}
variable "instance_type" {}
variable "vpc_security_group_id" {}
variable "identity" {}
variable "region" {}
variable "access_key" {}
variable "secret_key" {}
variable "runner_token" {}
variable "key_name" {}

provider "aws" {
access_key = "${var.access_key}"
secret_key = "${var.secret_key}"
region = "${var.region}"
}

module "runner" {
  source                 = "./runner"
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_id = "${var.vpc_security_group_id}"
  identity               = "${var.identity}"
  runner_token           = "${var.runner_token}"
  key_name               = "${var.key_name}"
  region                 = "${var.region}"
}

output "public_ip" {
  value = "${module.runner.public_ip}"
}

output "public_dns" {
  value = "${module.runner.public_dns}"
}